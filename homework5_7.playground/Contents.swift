import UIKit

class Shape {
    func calculateArea() -> Double {
        fatalError("Not implemented")
    }
    
    func calculatePerimeter() -> Double {
        fatalError("Not implemented")
    }
}

class Rectangle: Shape {
    private let height: Double
    private let width: Double
    
    init(height: Double, width: Double) {
        self.height = height
        self.width = width
    }
    
    override func calculateArea() -> Double {
        return height * width
    }
    
    override func calculatePerimeter() -> Double {
        return height * 2 + width * 2
    }
}

class Circle: Shape {
    private let radius: Double
    
    init(radius: Double) {
        self.radius = radius
    }
    
    override func calculateArea() -> Double {
        return Double.pi * pow(radius, 2)
    }
    
    override func calculatePerimeter() -> Double {
        return Double.pi * 2 * radius
    }
}

class Square: Shape {
    private let side: Double
    
    init(side: Double) {
        self.side = side
    }
    
    override func calculateArea() -> Double {
        return pow(side, 2)
    }
    
    override func calculatePerimeter() -> Double {
        return 4 * side
    }
}

let shapes: [Shape] = [Rectangle(height: 5, width: 10), Circle(radius: 15), Square(side: 7)]
var areas = 0.0
var perimeters = 0.0

shapes.forEach { shape in
    areas += shape.calculateArea()
    perimeters += shape.calculatePerimeter()
}

print(areas, perimeters)

//2

func findIndexWithGenerics<T>(ofString valueToFind: T, in array: [T]) -> Int? where T: Equatable {
    for (index, value) in array.enumerated() {
        if value == valueToFind {
            return index
        }
    }
    return nil
}

let arrayOfString = ["кот", "пес", "лама", "попугай", "черепаха"]
let arrayOfDouble = [5.5, 3.1, 34.5, 5.0]
let arrayOfInt = [5, 52, 32, 12, 235]

if let foundIndex = findIndexWithGenerics(ofString: "черепаха", in: arrayOfString) {
print("Индекс: \(foundIndex)")
}

if let foundIndex = findIndexWithGenerics(ofString: 5.5, in: arrayOfDouble) {
print("Индекс: \(foundIndex)")
}

if let foundIndex = findIndexWithGenerics(ofString: 32, in: arrayOfInt) {
print("Индекс: \(foundIndex)")
}
