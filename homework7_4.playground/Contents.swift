import UIKit

//1

var numbers = [Int]()

var i = 0

while i <= 50 {
    if i % 2 == 0 {
        print(i)
        numbers.append(i)
    }
    i += 1
}

i = 0

while i <= 9 {
    print(numbers[i])
    i += 1
}

//2

enum Referies: String {
    case lelik
    case bolik
    case anabolik
}

var sportResults = [String:[Referies:Double]] ()

sportResults["Zaycev"] = [.lelik:4.9,.bolik:2.7,.anabolik:1.4]
sportResults["Volkov"] = [.lelik:3.8,.bolik:4.9,.anabolik:4.6]

for (_, result) in sportResults {
    var sum = 0.0
    for (_, estimation) in result {
        sum += estimation
    }
    let avgSum = sum/Double(result.keys.count)
    print(avgSum)
}
