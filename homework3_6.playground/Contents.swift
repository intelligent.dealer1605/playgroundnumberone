import Foundation

func fibonacciNumber(count: Int) -> [Int] {
    var storyPointsNumbers: [Int] = []
    var num1 = 0
    var num2 = 1
    
    for _ in 0..<count {
        num1 += num2
        num2 = num1 - num2
        storyPointsNumbers.append(num1)
    }
    
    return storyPointsNumbers
}

var resultOfFibonacciNumber = fibonacciNumber(count: 5)
print(resultOfFibonacciNumber)

resultOfFibonacciNumber = fibonacciNumber(count: 7)
print(resultOfFibonacciNumber)

let printArrayFor: ([Int]) -> Void = { array in
    for value in array {
        print(value)
    }
}
printArrayFor(resultOfFibonacciNumber)
