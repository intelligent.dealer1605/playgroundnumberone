import Foundation

// HW2_1

let milkmanPhrase: String = "Молоко - это полезно"

print(milkmanPhrase)

//HW2_2

var milkPrice: Double = 3

//HW2_3

milkPrice = 4.20

//HW2_4

var milkBottleCount: Int? = 20
var profit: Double = 0.0

if let milkBottleCount = milkBottleCount {
    profit = milkPrice * Double(milkBottleCount)
}
/*
Напишите, почему не нужно использовать принудительное развертывание (force unwrap ) -
        будет краш, если значение у milkBottleCount пустое
 
пример:
var array: [Int] = [1, 2, 3]
let newValue: Int? = nil
array.append(newValue)
 */

//HW2_5

var employeesList: [String] = []
employeesList.append(contentsOf: ["Петр", "Геннадий", "Андрей", "Иван", "Марфа"])

//HW2_6

var isEveryoneWorkHard: Bool = false
let workingHours: Int = 35

if workingHours >= 40 {
    isEveryoneWorkHard = true
}
else {
    isEveryoneWorkHard = false
}

print(isEveryoneWorkHard)

