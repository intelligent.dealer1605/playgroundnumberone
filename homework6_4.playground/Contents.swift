import UIKit

//1

extension Int {
    
    func digitSum() -> Int {
        var n = self
        var sum = 0
        while n > 0 {
            sum += n % 10
            n /= 10
        }
        return sum
    }
}
125.digitSum()

//2
let numberFormatter = NumberFormatter()

extension String {
    
    func stringParserToInt() -> Int {
        let myString = self
        if let numInt = numberFormatter.number(from: myString) {
            let myInt = numInt.intValue
            return myInt
        }
        else {
            return 0
        }
    }
}

"test".stringParserToInt()

//3

protocol Operation {
    func calculate() -> Double
}

class Multiplication: Operation {
    
    private let num1: Double
    private let num2: Double
    
    init(num1: Double, num2: Double) {
        self.num1 = num1
        self.num2 = num2
    }
    
    func calculate() -> Double {
        return num1 * num2
    }
}

class Division: Operation {
    
    private let num1: Double
    private let num2: Double
    
    init(num1: Double, num2: Double) {
        self.num1 = num1
        self.num2 = num2
    }
    
    func calculate() -> Double {
        return num1 / num2
    }
}

let division = Division(num1: 10, num2: 3)
let multiplication  = Multiplication(num1: 55.2, num2: 5.0)

class Calculator {
    
    func calculateOperation(object: Operation) -> Double {
        return object.calculate()
    }
    
}

let calculator = Calculator()

calculator.calculateOperation(object: division)
calculator.calculateOperation(object: multiplication)
