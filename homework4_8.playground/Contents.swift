import UIKit

class FormBlock {
    
    enum Priority: String {
        case high
        case middle
        case low
    }
    
    let nameFieldHeader: String
    let nameFieldLength: Int
    private(set) var nameFieldPlaceholder: String?
    let nameFieldCode: Int?
    let nameFieldPriority: Priority
    
    init(nameFieldHeader: String, nameFieldLength: Int, nameFieldPlaceholder: String? = nil, nameFieldCode: Int? = nil, nameFieldPriority: Priority) {
        self.nameFieldHeader = nameFieldHeader
        self.nameFieldLength = nameFieldLength
        self.nameFieldPlaceholder = nameFieldPlaceholder
        self.nameFieldCode = nameFieldCode
        self.nameFieldPriority = nameFieldPriority
    }
    
    func validLengthOfField() -> Bool {
        nameFieldLength <= 25
    }
    
    func changePlaceholder(newValue: String?) {
        nameFieldPlaceholder = newValue
    }
}

let name = FormBlock(nameFieldHeader: "Name", nameFieldLength: 25, nameFieldPlaceholder: "Type your name", nameFieldCode: 1, nameFieldPriority: .high)
let surname = FormBlock(nameFieldHeader: "Surname", nameFieldLength: 25, nameFieldPlaceholder: "Type your surname", nameFieldCode: 2, nameFieldPriority: .high)
let age = FormBlock(nameFieldHeader: "Age", nameFieldLength: 3, nameFieldCode: 3, nameFieldPriority: .middle)
let city = FormBlock(nameFieldHeader: "City", nameFieldLength: 15, nameFieldPlaceholder: "City", nameFieldPriority: .low)
let notValidLength = FormBlock(nameFieldHeader: "random", nameFieldLength: 30, nameFieldPriority: .low)

print(city.validLengthOfField())
print(notValidLength.validLengthOfField())

//#5

struct Form {
    
    let nameFieldHeader: String
    let nameFieldLength: Int
    private(set) var nameFieldPlaceholder: String?
    let nameFieldCode: Int?
    
    func validLengthOfField() -> Bool {
        nameFieldLength <= 25
    }
    
    mutating func changePlaceholder(newValue: String?) {
        nameFieldPlaceholder = newValue
    }
}

let nameForm = Form(nameFieldHeader: "Name", nameFieldLength: 20, nameFieldCode: 5)
var surnameForm = Form(nameFieldHeader: "Surname", nameFieldLength: 30, nameFieldPlaceholder: "Surname", nameFieldCode: 6)
